package Client;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Main {
    public static void main(String []args) {
        try {
            int port = 1234;
            String host = "192.168.0.12";

            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, null, null);
            SSLSocketFactory factory = context.getSocketFactory();
            String[] suites = factory.getSupportedCipherSuites();
            SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
            socket.setEnabledCipherSuites(suites);
            System.out.println(socket.getSession().getCipherSuite());

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            printWriter.println("GyungDal");
            System.out.println(bufferedReader.readLine());
            System.out.println(bufferedReader.readLine());

            System.out.println("system Exit");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}