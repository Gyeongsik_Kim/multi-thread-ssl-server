package Server;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class Main {
    public static void main(String []args) {
        try {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, null, null);
            SSLServerSocketFactory factory = context.getServerSocketFactory();

            String [] suites = factory.getSupportedCipherSuites();
            SSLServerSocket serverSocket = (SSLServerSocket) factory.createServerSocket(1234);
            serverSocket.setEnabledCipherSuites(suites);


            int id = 0;
            while(true) {
                SSLSocket client = (SSLSocket) serverSocket.accept();
                System.out.println("Spawning client " + id);
                Thread clientThread = new Thread(new Server(client, id));
                clientThread.start();
                id = (id == Integer.MAX_VALUE - 1) ? 0 : id + 1;
                //오버플로우가 나기 전에 0으로 초기화
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
