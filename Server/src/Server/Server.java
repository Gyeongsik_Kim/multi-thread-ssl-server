package Server;

import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * Created by GyungDal on 2016-08-05.
 */

class Server extends Thread {
    private int id;
    private SSLSocket client;

    public Server(SSLSocket socket, int i) {
        client = socket;
        id = i;
    }

    public void run() {
        try {
            PrintWriter writer = new PrintWriter(client.getOutputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            writer.println("Your Client ID = " + id);
            String text;
            text=bufferedReader.readLine();
            System.out.println(text);
            System.out.println("ID : " + id + ", DATA " + text);
            long test = 0;
            for(int i = 0;i<10000000;i++){
                test += i;
            }
            Thread.sleep(20);
            writer.println("ID : " + id + ", DATA " + text + " RESULT " + test);

            writer.close();
            client.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
